import os
import shutil
import json
from io import BytesIO
import tqdm
import numpy as np
import xarray as xr
from scipy.spatial import KDTree
from PIL import Image
import itertools
import fsspec

from lltiler.lltiler import resolution2zoom, render_tile, numTiles

from .store_helper import get_store_options, initialize_store

def ll2xyz(lat, lon):
    lat = np.deg2rad(lat)
    lon = np.deg2rad(lon)
    return np.stack([np.cos(lat) * np.cos(lon), np.cos(lat) * np.sin(lon), np.sin(lat)], axis=-1)


class TileIndexBuilder:
    def __init__(self, xyz_coords):
        self.tree = KDTree(xyz_coords)

    def ll2index(self, lat, lon):
        xyz = ll2xyz(lat, lon)
        d, i = self.tree.query(xyz, workers=-1)
        return i

    def generate_index(self, level, tilesize):
        idxs = np.zeros((numTiles(level), numTiles(level), tilesize, tilesize), dtype="u4")
        for i in tqdm.tqdm(list(range(numTiles(level)))):
            for j in range(numTiles(level)):
                idxs[i, j] = render_tile(i, j, level, self.ll2index, tilesize)

        return xr.Dataset({
        f"{self.element_type}_of_pixel": (("tx", "ty", "y", "x"), idxs, {
            "start_index": 0,
            "long_name": f"0-based index of nearest neighbor grid {self.element_type} to map tile",
        })},
        attrs={
            "tile_level": level,
            "tilesize": tilesize,
        })


class CellTileIndexBuilder(TileIndexBuilder):
    element_type = "cell"
    def __init__(self, grid):
        super().__init__(np.stack([grid["cell_circumcenter_cartesian_" + i] for i in "xyz"], axis=1))


class VertexTileIndexBuilder(TileIndexBuilder):
    element_type = "vertex"
    def __init__(self, grid):
        super().__init__(np.stack([grid[f"cartesian_{i}_vertices"] for i in "xyz"], axis=1))

        
# ---


def sel_2nd(tiles, ix, iy):
    return (tiles
            .isel(tx=slice(ix, None, 2), ty=slice(iy, None, 2))
            .assign_coords(tx=tiles.tx[::2]//2, ty=tiles.ty[::2]//2))


def pyramid_step(tiles):
    return (xr.concat([xr.concat([sel_2nd(tiles, 0, 0), sel_2nd(tiles, 1, 0)], dim="x"),
                       xr.concat([sel_2nd(tiles, 0, 1), sel_2nd(tiles, 1, 1)], dim="x")],
                      dim="y")
            .coarsen(x=2, y=2)
            .mean()
            .assign_coords(x=tiles.x, y=tiles.y)
            .chunk({"tx": 4, "ty": 4, "x": len(tiles.x), "y": len(tiles.y)}))


def store_raw_tiles(tiles, folder):
    tileds = xr.Dataset({"tiles": tiles})
    level = int(np.log2(tileds.dims["tx"]))
    print("storing raw tiles")
    while level >= 0:
        filename = os.path.join(folder, f"{level}.zarr")
        print(f".. level {level}")
        tileds.to_zarr(filename, mode="w")
        if level > 0:
            tileds = pyramid_step(xr.open_zarr(filename, chunks={"tx": 8, "ty": 8}))
        level = level - 1


def build_raw_tiles(var, index, folder):
    values = var.values
    
    def get(x):
        return xr.DataArray(values[x.values], dims=x.dims)
    
    tiles = (xr.map_blocks(get, index)
             .assign_coords(**{name: np.arange(size)
                               for name, size in zip(index.dims, index.shape)})
             .assign_attrs(var.attrs))
    
    store_raw_tiles(tiles, folder)

def grouper(n, iterable):
    """
    >>> list(grouper(3, 'ABCDEFG'))
    [['A', 'B', 'C'], ['D', 'E', 'F'], ['G']]
    """
    iterable = iter(iterable)
    return iter(lambda: list(itertools.islice(iterable, n)), [])
    
    
def build_image_tiles(rawfolder, targetfolder, norm, cmap, maxlevel, store_options=None):
    if store_options is None:
        store_options = {}
    print("storing image tiles")
    mapper = fsspec.get_mapper(targetfolder, **store_options)
    for level in range(maxlevel+1):
        def store_images(block):
            def gen_blocks():
                for itx, tx in enumerate(block.tx.values):
                    for ity, ty in enumerate(block.ty.values):
                        name = f"{level}/{tx}/{ty}.jpg"
                        im = Image.fromarray(cmap(norm(block.isel(tx=itx, ty=ity)), bytes=True)[..., :3])
                        bio = BytesIO()
                        im.save(bio, format="JPEG")
                        yield name, bio.getvalue()
            for group in grouper(64, gen_blocks()):
                mapper.setitems(dict(group))
            return block.tx * block.ty

        print(f".. level {level}")
        ds = xr.open_zarr(os.path.join(rawfolder, f"{level}.zarr"))
        run = xr.map_blocks(store_images, ds.tiles)
        run.compute()

        
        
def parse_icon_time(t):
    import numpy as np
    fraction_of_day = t % 1
    daystr = str(int(t))
    return np.datetime64(daystr[0:4] + "-" + daystr[4:6] + "-" + daystr[6:8]) \
         + np.timedelta64(1, "s") * np.round(24*60*60 * fraction_of_day)

def fix_icon_netcdf(ds):
    if "time" in ds and ds.time.dtype.kind != "M":
        ds = ds.assign_coords(time=[parse_icon_time(t) for t in ds.time.values])
    rename_candidates = {"ncells": "cell"}
    ds = ds.rename({k: v for k, v in rename_candidates.items() if k in ds.dims}) \
           .squeeze([d for d, s in ds.dims.items() if d.startswith("height") and s == 1])
    return ds

def make_colormap(norm, cmap, name, units, horizontal=True):
    import matplotlib.pylab as plt
    import matplotlib.cm as cm

    if horizontal:
        fig, ax = plt.subplots(figsize=(8, 1))
        orientation = "horizontal"
    else:
        fig, ax = plt.subplots(figsize=(1.5, 8))
        orientation = "vertical"

    fig.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap),
                 cax=ax, orientation=orientation, label=f"{name} / {units}")
    fig.tight_layout()
    return fig


def store_colormaps(targetfolder, vmin, vmax, norm, cmap, name, units, store_options=None):
    if store_options is None:
        store_options = {}

    mapper = fsspec.get_mapper(targetfolder, **store_options)

    files = {}
    for filename, is_horizontal in [("cmap_h.png", True), ("cmap_v.png", False)]:
        fig = make_colormap(norm, cmap, name, units, is_horizontal)
        bio = BytesIO()
        fig.savefig(bio, format="PNG")
        files[filename] = bio.getvalue()

    files["varinfo.json"] = json.dumps({
        "name": name,
        "units": units,
        "vmin": vmin,
        "vmax": vmax,
        "cmap": cmap.name,
    }).encode("utf-8")

    mapper.setitems(files)

# vmin, vmax, colormap
default_plot_settings = {
    "sfcwind": (0, 25, "RdYlBu_r"),
    "tas": (260, 310, "turbo"),
    "prw": (0, 70, "BrBG"),
    "hus": (0, 0.02, "BrBG"),
    "rlut": (80, 380, "RdYlBu_r"),
    "vort": (-0.00005, 0.00005, "RdYlBu_r"),
}


def run(args):
    import matplotlib as mpl
    from dask.diagnostics import ProgressBar
    
    initialize_store(args.target)

    vmin = args.vmin if args.vmin is not None else default_plot_settings[args.variable][0]
    vmax = args.vmax if args.vmax is not None else default_plot_settings[args.variable][1]
    cmap = args.cmap if args.cmap is not None else default_plot_settings[args.variable][2]

    if args.symlog is None:
        norm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
    else:
        norm = mpl.colors.SymLogNorm(args.symlog, vmin=vmin, vmax=vmax)

    try:
        cmap = getattr(mpl.cm, cmap)
    except AttributeError:
        import cmocean
        cmap = getattr(cmocean.cm, cmap)
    
    data = xr.open_dataset(args.datafile).pipe(fix_icon_netcdf)
    index = xr.open_zarr(args.indexfile)
    assert data.uuidOfHGrid == index.uuidOfHGrid, "data grid and index grid don't match"
    level = index.tile_level
    target = args.target
    if not target.endswith("/"):
        target = target + "/"

    if args.location is None:
        dims = data[args.variable].dims
        if "cell" in dims:
            location = "cell"
        elif "vertex" in dims:
            location = "vertex"
        else:
            raise ValueError("can't guess location, please specify!")
    else:
        location = args.location

    store_options = get_store_options(target)

    da = data[args.variable]
    if "time" in da.dims:
        auxpath = f"l{level}/{args.variable}"
        def variter():
            for var in da.transpose("time", ...):
                time = str(plotvar.time.values.astype("datetime64[s]"))
                print(args.variable, time)
                tileset = f"l{level}/{args.variable}/{time}"
                yield tileset, var
    else:
        auxpath = f"l{level}/_fixed_time/{args.variable}"
        def variter():
            print(args.variable, "is fixed in time")
            tileset = f"l{level}/_fixed_time/{args.variable}"
            yield tileset, da

    store_colormaps(target + auxpath,
                    vmin, vmax, norm, cmap,
                    name=data[args.variable].attrs.get("long_name", args.variable),
                    units=data[args.variable].attrs.get("units", "-"),
                    store_options=store_options)
    
    for tileset, plotvar in variter():
        rawfolder = os.path.join(args.scratch, "rawtiles", tileset)
        os.makedirs(rawfolder, exist_ok=True)
        build_raw_tiles(plotvar, index[f"{location}_of_pixel"], rawfolder)
        
        target_path = target + tileset
        
        with ProgressBar():
            build_image_tiles(rawfolder, target_path, norm, cmap, level, store_options=store_options)
        
        shutil.rmtree(rawfolder)

def configure_tiler_parser(parser):
    parser.add_argument("datafile")
    parser.add_argument("variable")
    parser.add_argument("indexfile")
    parser.add_argument("target")
    parser.add_argument("-s", "--scratch", default=f"/scratch/{os.environ['USER'][0]}/{os.environ['USER']}")
    parser.add_argument("--location", default=None, type=str, help="location of data (cell or vertex)")
    parser.add_argument("--vmin", default=None, type=float, help="minimum data value")
    parser.add_argument("--vmax", default=None, type=float, help="maximum data value")
    parser.add_argument("--cmap", default=None, type=str, help="colormap")
    parser.add_argument("--symlog", default=None, type=float, help="use symmetric logarithmic norm, this is the threshold for linear values")
    parser.set_defaults(func=run)


def run_index_builder(args):
    grid = xr.open_dataset(args.gridfile)
    tiler = {
        "cell": CellTileIndexBuilder,
        "vertex": VertexTileIndexBuilder,
    }[args.location](grid)
    idx = tiler.generate_index(args.level, args.tilesize).assign_attrs(uuidOfHGrid=grid.uuidOfHGrid)
    idx.to_zarr(args.indexfile,
                encoding={f"{args.location}_of_pixel": {"chunks": [4, 4, args.tilesize, args.tilesize]}})


def configure_index_builder_parser(parser):
    parser.add_argument("gridfile", help="ICON gridfile (input)")
    parser.add_argument("indexfile", help="index file for tile generation (output, zarr)")
    parser.add_argument("-l", "--level", default=7, type=int, help="maximum zoom level for tiles")
    parser.add_argument("-s", "--tilesize", default=512, type=int, help="size of generated tiles in pixels")
    parser.add_argument("--location", default="cell", type=str, help="location of data (cell or vertex)")
    parser.set_defaults(func=run_index_builder)

        
if __name__ == "__main__":
    exit(main())

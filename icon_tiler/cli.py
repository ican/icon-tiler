from .icon_tiler import configure_tiler_parser, configure_index_builder_parser
from .layer_index import configure_layer_index_parser


def get_parser():
    import argparse

    parser = argparse.ArgumentParser(
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.set_defaults(func=None)
    subparsers = parser.add_subparsers()
    configure_index_builder_parser(subparsers.add_parser('make-index'))
    configure_tiler_parser(subparsers.add_parser('make-tiles'))
    configure_layer_index_parser(subparsers.add_parser('gather-layer-index'))

    return parser

def main():
    args = get_parser().parse_args()

    if not args.func:
        print("please provide a subcommand, run with -h for help", file=sys.stderr)
        return -1

    return(args.func(args))

if __name__ == "__main__":
    exit(main())

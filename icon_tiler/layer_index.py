import json
import fsspec
from .store_helper import get_store_options, initialize_store

def gather_index(targetfolder, tilesize, level=None, attribution=None, public_url=None, store_options=None):
    if store_options is None:
        store_options = {}

    if level is None:
        levelfolder = targetfolder.split("/")[-1]
        if levelfolder.startswith("l"):
            level = int(levelfolder[1:])
        else:
            raise ValueError("can't guess level, please specify using --level")

    initialize_store(targetfolder)

    def make_public(url):
        if public_url is None:
            return url
        else:
            return url.replace(targetfolder, public_url)

    fs, token, paths = fsspec.get_fs_token_paths(targetfolder, storage_options=store_options)
    path = paths[0]

    variables = {}
    for entry in fs.ls(path, detail=True):
        if entry["type"] != "directory":
            continue

        varname = entry["name"].split("/")[-1]

        if varname == "_fixed_time":
            continue

        varpath = entry["name"]
        times = []
        for entry2 in fs.ls(entry["name"], detail=True):
            if entry2["type"] == "directory":
                times.append(entry2["name"].split("/")[-1])

        cmaps = {}
        for direction, suffix in [("horizontal", "cmap_h.png"), ("vertical", "cmap_v.png")]:
            path = varpath + "/" + suffix
            if fs.exists(path):
                cmaps[direction] = make_public(path)

        varinfo = {"path": make_public(varpath), "times": list(sorted(times)), "cmaps": cmaps}
        try:
            varinfo["info"] = json.loads(fs.cat(varpath + "/varinfo.json"))
        except OSError:
            pass

        variables[varname] = varinfo

    path = paths[0]
    fixed_time_variables = {}
    for entry in fs.ls(path + "/_fixed_time", detail=True):
        if entry["type"] != "directory":
            continue

        varname = entry["name"].split("/")[-1]
        varpath = entry["name"]

        cmaps = {}
        for direction, suffix in [("horizontal", "cmap_h.png"), ("vertical", "cmap_v.png")]:
            path = varpath + "/" + suffix
            if fs.exists(path):
                cmaps[direction] = make_public(path)

        varinfo = {"path": make_public(varpath), "cmaps": cmaps}
        try:
            varinfo["info"] = json.loads(fs.cat(varpath + "/varinfo.json"))
        except OSError:
            pass

        fixed_time_variables[varname] = varinfo


    mapper = fsspec.get_mapper(targetfolder, storage_options=store_options)
    mapper["index.json"] = json.dumps({"variables": variables,
                                       "fixed_time_variables": fixed_time_variables,
                                       "max_level": level,
                                       "tilesize": tilesize,
                                       "attribution": attribution or ""}).encode("utf-8")

    print(make_public(targetfolder + "/index.json"))




def run(args):
    gather_index(args.target, tilesize=args.tilesize, level=args.level, attribution=args.attribution,
                 public_url=args.public_url, store_options=get_store_options(args.target))
    

def configure_layer_index_parser(parser):
    parser.add_argument("target")
    parser.add_argument("--public-url", default=None, type=str, help="publicly accessible path to 'target'")
    parser.add_argument("-l", "--level", default=None, type=int, help="maximum zoom level for tiles")
    parser.add_argument("-s", "--tilesize", default=512, type=int, help="size of generated tiles in pixels")
    parser.add_argument("--attribution", default="MPIM ICON", type=str, help="dataset attribution")
    parser.set_defaults(func=run)

async def get_client(**kwargs):
    import aiohttp
    import aiohttp_retry
    retry_options = aiohttp_retry.ExponentialRetry(
            attempts=3,
            exceptions={OSError, aiohttp.ServerDisconnectedError})
    retry_client = aiohttp_retry.RetryClient(raise_for_status=False, retry_options=retry_options)
    return retry_client


def get_store_options(path):
    if path.startswith("swift:"):
        return {"get_client": get_client}
    else:
        return {"auto_mkdir": True}


def parse_swiftenv():
    import os
    for line in open(os.path.expanduser("~/.swiftenv")):
        if line.startswith("#"):
            continue
        parts = line.split()
        if parts[0] == "setenv":
            os.environ[parts[1]] = parts[2].strip("\"")
        else:
            parts = line.split("=", 1)
            if len(parts) == 2:
                if parts[0].startswith("export "):
                    parts[0] = parts[0][7:]
                os.environ[parts[0].strip()] = parts[1].strip().strip("\"")

def initialize_store(path):
    if path.startswith("swift:"):
        parse_swiftenv()

